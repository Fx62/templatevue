# DB

## Crear db
```
CREATE DATABASE APP;
```

## Acceder a db
```
USE APP;
```

## Crear tabla
USUARIOS
```
CREATE TABLE USER
(
id int auto_increment,
username varchar(20),
first_name varchar(50),
last_name varchar(50),
password varchar(32),
admin int(1)
, PRIMARY KEY (id));

```
```
ALTER TABLE USER
ADD CONSTRAINT UQ_USER
UNIQUE KEY(username);
```

BITACORA
```
CREATE TABLE BITACORA_ADMIN 
(
id INT auto_increment,
nombre_tabla varchar(30), 
tipo_operacion varchar(110),
fecha_hora datetime,
USER_id	int,
PRIMARY KEY(id)
);
```
```
ALTER TABLE BITACORA_ADMIN
ADD CONSTRAINT FK_BITACORA_ADMIN_USER
FOREIGN KEY (USER_id) 
REFERENCES USER(id)
ON DELETE CASCADE;
```

REGISTROS
```
CREATE TABLE REGISTRO
(
id int auto_increment,
file_name varchar(100),
fecha_hora datetime,
USER_id int,
PRIMARY KEY(id)
);
```
```
ALTER TABLE REGISTRO
ADD CONSTRAINT FK_REGISTRO_USER
FOREIGN KEY (USER_id) 
REFERENCES USER(id)
ON DELETE CASCADE;
```


[Documentacion Back-end](https://gitlab.com/Fx62/templatevue/blob/master/back/README.md)

[Documentacion Front-end](https://gitlab.com/Fx62/templatevue/blob/master/front/README.md)
