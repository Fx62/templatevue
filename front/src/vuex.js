import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

/*const state = {
    user: ''
};

const store = new Vuex.Store({
    state,
    getters: {
        user: (state) =>{
            return state.user;
        }
    },
    actions: {
        user(context, user){
            context.commit ('user', user);
        }
    },
    mutations: {
        user(state, user){
            state.user = user;
        }
    }
});*/

const state = {
    user: '',
    admin: 0,
    id: 0
};

const store = new Vuex.Store({
    state,
    getters: {
        user: (state) =>{
            return state.user;
        },
        admin: (state) =>{
            return state.admin
        },
        id: (state) =>{
            return state.id
        }
    },
    actions: {
        user(context, user){
            context.commit ('user', user);
        },
        admin(context, admin){
            context.commit ('admin', admin);
        },
        id(context, id){
            context.commit ('id', id);
        }
    },
    mutations: {
        user(state, user){
            state.user = user;
        },
        admin(state, admin){
            state.admin = admin;
        },
        id(state, id){
            state.id = id;
        }
    }, 
});


export default store;