import axios from "axios";

export default {
  addLog: async function (table, operation, user) {
    var currentdate = new Date();
    var formatDate =
      currentdate.getFullYear() +
      "-" +
      (currentdate.getMonth() + 1) +
      "-" +
      currentdate.getDate() +
      " " +
      currentdate.getHours() +
      ":" +
      currentdate.getMinutes() +
      ":" +
      currentdate.getSeconds();
    await axios.post("logs/add", {
      nombre_tabla: table,
      tipo_operacion: operation,
      fecha_hora: formatDate,
      USER_id: user,
    });
  },
  userLogged: function (token) {
    if (token) {
      var base64Url = token.split('.')[1];
      var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
      return JSON.parse(jsonPayload).user
    } else {
      return {
        full_name: "",
        admin: 0
      }
    }
  },
  sleep: function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}