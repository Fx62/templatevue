import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Files from './components/Files.vue'
import Users from './components/Users.vue'
import Edit from './components/Edit.vue'
import Logs from './components/Log.vue'

Vue.use(Router)

export default new Router({
    mode: 'history', // para no mostrar # en url
    routes: [
        { path: '/', component: Files },
        { path: '/login', component: Login },
        { path: '/register', component: Register },
        { path: '/users', component: Users },
        { path: '/user/:id', component: Edit },
        { path: '/logs', component: Logs }
    ]
})