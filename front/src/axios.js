import axios from 'axios'

//let token = localStorage.getItem('token')
//axios.defaults.baseURL = 'http://localhost:3000/api/';
//axios.defaults.headers.Authorization = 'Bearer ' + localStorage.getItem('token');

axios.interceptors.request.use(function (config) {
    config.baseURL = 'http://localhost:3000/api/';
    config.headers.authorization = 'Bearer ' + localStorage.getItem('token');
    
    return config;
  }, function (error) {
    return Promise.reject(error);
  });