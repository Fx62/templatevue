const mysql = require('mysql');
var CryptoJS = require("crypto-js")

const pool = mysql.createPool({
    connectionLimit: 10,
    password: 'Th1sIsS3cr3T',
    user: 'root',
    database: 'APP',
    host: '0.0.0.0',
    port: 3306
});

let appDb = {};

// users methods

appDb.allUsers = () => {
    return new Promise((resolv, reject) => {
        pool.query('SELECT id, username, first_name, last_name, admin FROM USER', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

appDb.login = (username, password) => {
    return new Promise((resolv, reject) => {
        pass = CryptoJS.MD5(password);
        pool.query('SELECT id, username, first_name, last_name, admin FROM USER WHERE username = ? AND password = ?', [username, pass.toString()], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

appDb.signup = (username, first_name, last_name, password, admin) => {
    return new Promise((resolv, reject) => {
        pass = CryptoJS.MD5(password);
        pool.query('INSERT INTO USER(username, first_name, last_name, password, admin) values(?, ?, ?, ?, ?)', [username, first_name, last_name, pass.toString(), admin], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

//UPDATE USER SET admin=1 WHERE id = 29
appDb.updatePermissions = (id, admin) => {
    return new Promise((resolv, reject) => {
        pool.query('UPDATE USER SET admin=? WHERE id = ?', [admin, id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

// UPDATE USER SET first_name="Fernando", last_name="Rodriguez" where id =40
appDb.updateUser = (id, first_name, last_name, password) => {
    return new Promise((resolv, reject) => {
        if (password) {
            pass = CryptoJS.MD5(password);
            pool.query('UPDATE USER SET first_name = ?, last_name = ?, password = ? WHERE id = ?', [first_name, last_name, pass.toString(), id], (err, results) => {
                if (err) {
                    return reject(err);
                }
                return resolv(results);
            });
        } else {
            pool.query('UPDATE USER SET first_name = ?, last_name = ? WHERE id = ?', [first_name, last_name, id], (err, results) => {
                if (err) {
                    return reject(err);
                }
                return resolv(results);
            });
        }
    })
};

appDb.get = (id) => {
    return new Promise((resolv, reject) => {
        pool.query('SELECT id, username, first_name, last_name FROM USER WHERE id = ?', [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

appDb.deleteUser = (id) => {
    return new Promise((resolv, reject) => {
        pool.query('DELETE FROM USER WHERE id = ?', [id], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

// registers methos
//  FUNCION SELECCIONAR TODOS
appDb.allRegistro = () => {
    return new Promise((resolv, reject) => {
        pool.query('SELECT R.id, R.file_name, R.fecha_hora, U.username, R.USER_id FROM REGISTRO R INNER JOIN USER U ON R.USER_id = U.id', (err, results) => {
            if(err){
                return reject(err);
            }
            return resolv(results);
        });
    })
};

// FUNCION BUSCAR POR ID

appDb.oneRegistro = (id) => {
    return new Promise((resolv, reject) => {
        pool.query('SELECT id, file_name, fecha_hora, USER_id FROM REGISTRO WHERE id = ?', [id], (err, results) => {
            if(err){
                return reject(err);
            }
            return resolv(results);
        });
    })
};

// FUNCION AGREGAR

appDb.addRegistro = (file_name, fecha_hora, USER_id) => {
    return new Promise((resolv, reject) => {
        pool.query('INSERT INTO REGISTRO(file_name, fecha_hora, USER_id) values(?, ?, ?)', [file_name, fecha_hora, USER_id], (err, results) => {
            if(err){
                return reject(err);
            }
            return resolv(results);
        });
    })
};

// FUNCION BORRAR

appDb.deleteRegistro = (id) => {
    return new Promise((resolv, reject) => {
        pool.query('DELETE FROM BITACORA_ADMIN WHERE BITACORA_ADMIN = ?', [id])
        pool.query('DELETE FROM REGISTRO WHERE id = ?', [id], (err, results) => {
            if(err){
                return reject(err);
            }
            return resolv(results);
        });
    })
};


// log methods
appDb.allLogs = () => {
    return new Promise((resolv, reject) => {
        pool.query('SELECT B.id, B.nombre_tabla, B.tipo_operacion, B.fecha_hora, B.USER_id, U.username FROM BITACORA_ADMIN B INNER JOIN USER U ON B.USER_id = U.id ORDER BY B.id DESC', (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

appDb.addLog = (table, operation, datetime, user) => {
    //console.log(table + ' ' + operation + ' ' + datetime + ' ' + user)
    return new Promise((resolv, reject) => {
        pool.query('INSERT INTO BITACORA_ADMIN(nombre_tabla, tipo_operacion, fecha_hora, USER_id) VALUES(?, ?, ?, ?)', [table, operation, datetime, user], (err, results) => {
            if (err) {
                return reject(err);
            }
            return resolv(results);
        });
    })
};

module.exports = appDb;
