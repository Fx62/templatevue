const express = require('express')
const db = require('../db');
const jwt = require("jsonwebtoken")

const router = express.Router();
// upload files
const path = require('path');
const multer = require('multer');
const fs = require('fs');


//  users routes
router.get('/user/test', (req, res, next) => {
    res.json({ user: 'test' });
});

router.post('/user/login', async (req, res, next) => {
    try {
        let results = await db.login(req.body.username, req.body.password);
        //res.json(results) // sin token
        user = results[0];
        user.full_name = user.first_name + " " + user.last_name;
        delete user['first_name'];
        delete user['last_name'];
        jwt.sign({ user }, 'secretkey', { expiresIn: '60m' }, (err, token) => {
            res.json({ token });
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.post('/user/signup', async (req, res, next) => {
    try {
        let results = await db.signup(req.body.username, req.body.first_name, req.body.last_name, req.body.password, req.body.admin);
        res.json(results);
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.get('/user/', verifyToken, async (req, res, next) => {
    try {
        jwt.verify(req.token, 'secretkey', (error, authData) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(authData);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

//router.get('/user/all', async(req, res, next) =>{ // sin token
router.get('/user/all', verifyToken, async (req, res, next) => {
    try {
        let results = await db.allUsers();
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(req.token)
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.get('/user/:id', verifyToken, async (req, res, next) => {
    try {
        let results = await db.get(req.params.id);
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.put('/user/updatePermissions', verifyToken, async (req, res, next) => {
    try {
        let results = await db.updatePermissions(req.body.id, req.body.admin);
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.put('/user', verifyToken, async (req, res, next) => {
    try {
        let results = await db.updateUser(req.body.id, req.body.first_name, req.body.last_name, req.body.password);
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.delete('/user/:id', verifyToken, async (req, res, next) => {
    try {
        let results = await db.deleteUser(req.params.id);
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/register/test', verifyToken, (req, res, next) => {
    res.json({ resgister: 'test' });
});

// registers routes
// ------ API REGISTRO -----

// SELECCIONAR TODO
router.get('/registro/', verifyToken, async (req, res, next) => {
    try {
        jwt.verify(req.token, 'secretkey', (error, authData) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(authData);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/registro/all', verifyToken, async (req, res, next) => {
    try {
        let results = await db.allRegistro();
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(req.token)
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

// AGREGAR
router.post('/registro/add', async (req, res, next) => {
    try {
        let results = await db.addRegistro(req.body.file_name, req.body.fecha_hora, req.body.USER_id);
        res.json(results);
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

// BORRAR 

router.delete('/registro/:id', verifyToken, async (req, res, next) => {
    try {
        let results = await db.deleteRegistro(req.params.id);
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

// BUSCAR POR ID

router.get('/registroOne/:id', verifyToken, async (req, res, next) => {
    try {
        let results = await db.oneRegistro(req.params.id);
        //res.json(results) // sin token
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(req.token)
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.post('/registro/delete/:file_name', async (req, res, next) => {
    const fs = require('fs');
    // delete a file
    try {
        fs.unlinkSync('./subidas/' + req.params.file_name);

        console.log("File is deleted.");
    } catch (error) {
        console.log(error);
    }
});

// logs routes
router.get('/logs/all', verifyToken, async (req, res, next) => {
    try {
        let results = await db.allLogs();
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                //console.log(req.token)
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});


router.post('/logs/add', verifyToken, async (req, res, next) => {
    try {
        let results = await db.addLog(req.body.nombre_tabla, req.body.tipo_operacion, req.body.fecha_hora, req.body.USER_id);
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                //console.log(req.token)
                console.log(error)
            } else {
                res.json(results);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});


// Authorization: Bearer <token>
function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader !== 'undefined') {
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();
    } else {
        res.sendStatus(403);
    }
}

// upload files
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './subidas')
    },
    filename: (req, file, cb) => {
        var cadena = file.originalname.split(".");
        let date_ob = new Date();
        let date = ("0" + date_ob.getDate()).slice(-2);
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        let year = date_ob.getFullYear();
        let hours = date_ob.getHours();
        let minutes = date_ob.getMinutes();
        let seconds = date_ob.getSeconds();
        cb(null, cadena[0] + '_' + year + '-' + month + '-' + date + '_' + hours + ':' + minutes + ':' + seconds + path.extname(file.originalname));
    }
});

const upload = multer({ storage });
//router.use(express.json());
//router.use(express.urlencoded({extended:true}));

router.post('/subir', verifyToken, upload.single('file'), async (req, res) => {
    try {
        jwt.verify(req.token, 'secretkey', (error) => {
            if (error) {
                res.sendStatus(403);
                console.log(error)
            } else {
                console.log('file saved');
                return res.send(req.file);
            }
        });
    } catch (e) {
        console.log(e);
        res.sendStatus(500);
    }
});

router.get('/files', verifyToken, async (req, res, next) => {
    const path = './subidas'
    fs.readdir(path, function (err, files) {
        if (err) {
            res.status(500).send({
                message: "Unable to scan files!",
            });
        }
        jwt.verify(req.token, 'secretkey', (error) => {
            console.log(files)
            res.status(200).send(files);
        })
    });
});

const download = (req, res) => {
    const fileName = req.params.name;
    const directoryPath = './subidas/';

    res.download(directoryPath + fileName, fileName, (err) => {
        if (err) {
            res.status(500).send({
                message: "Could not download the file. " + err,
            });
        }
    });
};
router.get('/files/:name', download);

//-----------------



module.exports = router;