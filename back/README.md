# Back

## Actualizar credenciales de DB en archivo *server/db/index.js*
```
const pool = mysql.createPool({
    connectionLimit: 10,
    password: 'PASSWORD DE DB',
    user: 'USUARIO DE DB',
    database: 'NOMBRE DE DB',
    host: 'IP O NOMBRE DEL SERVIDOR DE DB',
    port: 3306 // PUERTO DE DB
});
```

## Ingresar a directorio de back-end
```
cd templatevue/back
```

## Preparar proyect
```
npm install
```

## Ejecutar back-end
```
npm run dev
```

## Validar api
```
http://localhost:3000/api/user/test
http://ip:3000/api/user/test
```
